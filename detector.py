import os
import sys
import cv2
import numpy as np
from PIL import Image
from keras_yolo3.yolo import YOLO
from timeit import default_timer as timer


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


if __name__ == '__main__':
	model_path = 'model/trained_weights_final.h5'
	anchors_path = 'data/yolo_anchors.txt'
	classes_path = 'model/data_classes.txt'
	score = 0.25
	gpu_num = 1

	# define YOLO detector
	yolo = YOLO(
		**{
			'model_path': model_path,
			'anchors_path': anchors_path,
			'classes_path': classes_path,
			'score': score,
			'gpu_num': gpu_num,
			'model_image_size': (416, 416),
		}
	)

	def detect_object(yolo, received_image):
		'''
		Call YOLO logo detector on input image, optionally save resulting image.

		Args:
		  yolo: keras-yolo3 initialised YOLO instance
		  img_path: path to image file
		Returns:
		  prediction: list of bounding boxes in format (xmin, ymin, xmax, ymax, class_id, confidence)
		  image: unaltered input image as (H, W, C) array
		'''
		try:
			image = Image.fromarray(received_image)
			image_array = np.array(image)
		except:
			print('File Open Error! Try again!')
			return None, None

		prediction, result_image = yolo.detect_image(image)
		print(f'\nci: {prediction[0]}\ngender: {prediction[1]}\ndata: {prediction[2]}')

		converted_image = np.array(result_image)
		result = converted_image[:, :, ::-1].copy()
		cv2.imwrite('result.png', result)

		return True

	# load our input image and grab its spatial dimensions
	img_path = cv2.imread(sys.argv[1])
	rgb = cv2.cvtColor(img_path, cv2.COLOR_BGR2RGB)

	if img_path is not False:
		# start = timer()
		predict = detect_object(yolo, rgb)
		# end = timer()

		if predict is True:
			pass
		else:
			print('Print some error here')

	# close the current yolo session
	yolo.close_session()
