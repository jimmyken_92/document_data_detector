#!/bin/bash
# trained_weights_final.h5
yolo_id="1Y_KU_Fk605J-mDl9Y41BA5fEhuBoW28_"
yolo_filename="trained_weights_final.h5"
curl -c ./cookie -s -L "https://drive.google.com/uc?export=download&id=${yolo_id}" > /dev/null
curl -Lb ./cookie "https://drive.google.com/uc?export=download&confirm=`awk '/download/ {print $NF}' ./cookie`&id=${yolo_id}" -o ${yolo_filename}
