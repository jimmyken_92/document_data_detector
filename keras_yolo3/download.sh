#!/bin/bash
# yolo.h5
yolo_id="1pG_m6AZVSeDSXyJAI7ie5MV_sbP0bAwi"
yolo_filename="yolo.h5"
curl -c ./cookie -s -L "https://drive.google.com/uc?export=download&id=${yolo_id}" > /dev/null
curl -Lb ./cookie "https://drive.google.com/uc?export=download&confirm=`awk '/download/ {print $NF}' ./cookie`&id=${yolo_id}" -o ${yolo_filename}

# yolo weights
weights_id="1wI1Qb3Sbo1iGBLzGIq0NjMZxmnCZIuxl"
weights_filename="yolov3.weights"
curl -c ./cookie -s -L "https://drive.google.com/uc?export=download&id=${weights_id}" > /dev/null
curl -Lb ./cookie "https://drive.google.com/uc?export=download&confirm=`awk '/download/ {print $NF}' ./cookie`&id=${weights_id}" -o ${weights_filename}
